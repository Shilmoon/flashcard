import SwiftUI

struct CustomButton: View {
    var iconName: String
    var buttonText: String
    var geometry: GeometryProxy
    var padding: CGFloat
    var action: () -> Void

    var body: some View {
        Button(action: action) {
            HStack {
                Text(buttonText)
                    .font(.system(size: geometry.size.width * 0.0185))
                Image(systemName: iconName)
                    .font(.system(size: geometry.size.width * 0.0185))
            }
        }
        .buttonStyle(PlainButtonStyle())
        .padding(.trailing, padding)
    }
}
