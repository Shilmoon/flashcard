import SwiftUI
struct UserInfoView: View {
    var username: String
    var email: String
    var geometry: GeometryProxy
    var showList: Binding<Bool>

    var body: some View {
        HStack(spacing: 1){
            VStack(alignment: .trailing, spacing: 5){
                Text("\(username)")
                    .font(.system(size: geometry.size.width * 0.0185))
                Text("\(email)")
                    .font(.system(size: geometry.size.width * 0.00925))
                    .foregroundStyle(Color.gray)
            }
            Button(action: {
                withAnimation(.easeInOut(duration: 0.3)){
                    showList.wrappedValue.toggle() // Toggle the visibility of the list
                }
            }) {
                Circle()
                    .fill(Color.accentColor)
                    .frame(width: geometry.size.width*0.047, height: geometry.size.height*0.075)
                    .overlay(Color.clear)
                    .padding()
            }
        }
    }
}
