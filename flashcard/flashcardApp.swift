//
//  flashcardApp.swift
//  flashcard
//
//  Created by jhon on 12.04.2024.
//

import SwiftUI

@main
struct flashcardApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
