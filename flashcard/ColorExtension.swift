import SwiftUI

extension Color {
    static let whitetext = Color(red: 235 / 255, green: 235 / 255, blue: 235 / 255)
    static let blockgray = Color(red: 47 / 255, green: 47 / 255, blue: 52 / 255)
    static let backgroundgray = Color(red: 37 / 255, green: 37 / 255, blue: 42 / 255)
    static let textblockgray = Color(red: 57 / 255, green: 57 / 255, blue: 62 / 255)
    static let accentyellow = Color(red: 255/255, green: 184/255, blue: 0/255)
    static let accentblue = Color(red: 73/255, green: 95/255, blue: 250/255)
    static let accentgray = Color(red: 68/255, green: 68/255, blue: 68/255)
}


