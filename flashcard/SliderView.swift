import SwiftUI

struct SliderView: View {
    @Binding var value: Double
    @State private var isEditing = false
    let range: ClosedRange<Double>
    let step: Double
    
    var body: some View {
        GeometryReader { geometry in
            VStack {
                HStack(){
                    Spacer()
                    Slider(value: $value, in: range, step: step,
                           onEditingChanged: { editing in
                        isEditing = editing
                    })            .frame(width: geometry.size.width*0.95, height: geometry.size.height*0.95)
                    
                    Spacer()
                }
            }
        }
    }
}
