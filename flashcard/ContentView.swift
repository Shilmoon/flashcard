import SwiftUI

struct ContentView: View {
    @State private var isHoveredLearn = false
    @State private var isHoveredReview = false
    @State private var showList = false
    @State private var number: Double = 0
    @State private var showSlider = false
    @State private var sliderValue: Double = 50
    let username = "user123"
    let email = "123123@mail.com"
    let dayData = [70, 20, 55, 20, 30, 75, 10]
    let currentDayIndex = 2
    let daysOfWeek = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat","Sun"]
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                Color(Color.backgroundgray).edgesIgnoringSafeArea(.all)
                VStack{
                    HStack(spacing: geometry.size.width * 0.172) {
                        Button(action: {
                            print("Button Tapped")
                        }) {
                            VStack {
                                Image(systemName: "plus.diamond").font(.system(size: geometry.size.width * 0.028))
                                Text("Learn new words")
                                    .font(.system(size: geometry.size.width * 0.028))
                            }
                            .frame(width: geometry.size.width * 0.278, height: geometry.size.height * 0.153)
                            .background(RoundedRectangle(cornerRadius: 10)
                                .fill(isHoveredLearn ? Color.accentColor : Color.blockgray))
                            .overlay(
                                RoundedRectangle(cornerRadius: 10)
                                    .stroke(Color.blockgray, lineWidth: 2)
                            )
                            .animation(.easeIn(duration: 0.3), value: isHoveredLearn)
                            .foregroundColor(Color.whitetext)
                        }
                        .accessibilityIdentifier("Learn new words")
                        .buttonStyle(PlainButtonStyle())
                        .onHover { over in
                            isHoveredLearn = over
                        }
                        
                        Button(action: {
                            print("Button Tapped")
                        }
                        ) {
                            VStack {
                                Image(systemName: "eye.circle").font(.system(size: geometry.size.width * 0.028))
                                Text("Review words")
                                    .font(.system(size: geometry.size.width * 0.028))
                            }
                            .frame(width: geometry.size.width * 0.278, height: geometry.size.height * 0.153)
                            .background(RoundedRectangle(cornerRadius: 10)
                                .fill(isHoveredReview ? Color.accentyellow : Color.blockgray))
                            .overlay(
                                RoundedRectangle(cornerRadius: 10)
                                    .stroke(Color.blockgray, lineWidth: 2)
                            )
                            .animation(.easeIn(duration: 0.3), value: isHoveredReview)
                            .foregroundColor(Color.whitetext)
                        }
                        .accessibilityIdentifier("Review words")
                        .buttonStyle(PlainButtonStyle())
                        .onHover { over in
                            isHoveredReview = over
                        }
                        
                    }
                    Rectangle()
                        .frame(width: geometry.size.width * 0.73, height: geometry.size.height * 0.371)
                        .foregroundColor(Color.blockgray)
                        .overlay(
                            VStack(alignment: .center, spacing: 80) {
                                HStack(alignment: .center,spacing: 153){
                                    HStack(alignment: .lastTextBaseline, spacing: 7) {
                                        Text("Daily goal")
                                            .font(.system(size: geometry.size.width * 0.0185))
                                            .foregroundColor(Color.whitetext)
                                            .accessibilityIdentifier("Edit Goal")
                                        Button(action: {
                                            self.showSlider.toggle()
                                        }) {
                                            Text("\(Int(number))").font(.system(size: geometry.size.width * 0.0185))
                                            Image(systemName: "rectangle.and.pencil.and.ellipsis").font(.system(size: geometry.size.width * 0.0185))
                                        }
                                        .accessibilityIdentifier("Edit Goal")
                                        .frame(width: geometry.size.width * 0.061, height: geometry.size.height * 0.05)
                                        .background(RoundedRectangle(cornerRadius: 10).fill(Color.textblockgray))
                                        .buttonStyle(PlainButtonStyle())
                                        .popover(isPresented: $showSlider) {
                                            SliderView(value: $number, range: 5...100, step: 20)
                                                .frame(width: 300, height: 50)
                                                .accessibilityIdentifier("goalSlider")
                                        }
                                    }
                                    HStack(alignment: .lastTextBaseline, spacing: 7) {
                                        Text("Current streak")
                                            .font(.system(size: geometry.size.width * 0.0185))
                                            .foregroundColor(Color.whitetext)
                                        Text("\(Int(number))").font(.system(size: geometry.size.width * 0.0185))
                                            .frame(width: geometry.size.width * 0.0367, height: geometry.size.height * 0.05)
                                            .background(RoundedRectangle(cornerRadius: 10).fill(colorForValue(number)))
                                    }
                                    HStack(alignment: .lastTextBaseline, spacing: 7) {
                                        Text("Max streak")
                                            .font(.system(size: geometry.size.width * 0.0185))
                                            .foregroundColor(Color.whitetext)
                                        Text("\(Int(number))").font(.system(size: geometry.size.width * 0.0185))
                                            .frame(width: geometry.size.width * 0.0367, height: geometry.size.height * 0.05)
                                            .background(RoundedRectangle(cornerRadius: 10).fill(colorForValue(number)))
                                    }
                                }
                                .padding(.top, 10)
                                HStack(spacing: 21) {
                                    ForEach(Array(zip(daysOfWeek.indices, zip(daysOfWeek, dayData))), id: \.1.0) { index, dayInfo in
                                        let (day, value) = dayInfo
                                        VStack {
                                            if index > currentDayIndex {
                                                Circle()
                                                    .strokeBorder(style: StrokeStyle(lineWidth: 2, dash: [5]))
                                                    .frame(width: geometry.size.width * 0.086, height: geometry.size.height * 0.1375)
                                                    .foregroundColor(.white)
                                            } else {
                                                Circle()
                                                    .trim(from: 0, to: CGFloat(value) / number)
                                                    .stroke(value >= Int(number) ? Color.green : Color.accentColor, lineWidth: geometry.size.width * 0.01)
                                                    .rotationEffect(.degrees(-90))
                                                    .frame(width: geometry.size.width * 0.086, height: geometry.size.height * 0.1375)
                                                    .overlay(
                                                        Text("\(value)")
                                                            .font(.system(size: geometry.size.width * 0.0185))
                                                            .foregroundColor(.white)
                                                    )
                                            }
                                            Text(day)
                                                .font(.system(size: geometry.size.width * 0.0185))
                                                .foregroundColor(.white)
                                        }
                                        .padding(.bottom, 20)
                                    }
                                }
                            }
                        )
                        .cornerRadius(10)
                        .padding(.top, geometry.size.height * 0.105)
                }
                
                HStack {
                    Spacer()
                    VStack{
                        UserInfoView(username: username, email: email, geometry: geometry, showList: $showList)
                            .buttonStyle(PlainButtonStyle())
                        Spacer()
                    }
                }
                
                if showList {
                    Rectangle()
                        .foregroundColor(.black.opacity(0.5))
                        .edgesIgnoringSafeArea(.all)
                        .transition(.opacity)
                        .onTapGesture {
                            showList.toggle()
                        }
                    
                    HStack {
                        Spacer()
                        VStack (alignment: .trailing){
                            UserInfoView(username: username, email: email, geometry: geometry, showList: $showList)
                                .buttonStyle(PlainButtonStyle())
                            Divider().offset(y:-10)
                            VStack(alignment: .trailing,spacing: 10) {
                                CustomButton(iconName: "globe.europe.africa", buttonText: "Languages", geometry: geometry, padding: 10) { }
                                CustomButton(iconName: "list.bullet.circle", buttonText: "Vocabulary", geometry: geometry, padding: 10) { }
                                CustomButton(iconName: "gear", buttonText: "Settings", geometry: geometry, padding: 10) { }
                                CustomButton(iconName: "doc.text", buttonText: "About", geometry: geometry, padding: 10) { }
                            }
                            Spacer()
                        }
                        .frame(width: geometry.size.width * 0.165)
                        .background(Color.accentgray)
                        .transition(.move(edge: .trailing))
                    }
                }
            }
        }
    }
    func colorForValue(_ value: Double) -> Color {
        let startColor = NSColor.blue
        let endColor = NSColor.red
        let ratio = value / 100
        let newRed = (1 - ratio) * Double(startColor.redComponent) + ratio * Double(endColor.redComponent)
        let newGreen = (1 - ratio) * Double(startColor.greenComponent) + ratio * Double(endColor.greenComponent)
        let newBlue = (1 - ratio) * Double(startColor.blueComponent) + ratio * Double(endColor.blueComponent)
        return Color(NSColor(red: newRed, green: newGreen, blue: newBlue, alpha: 1))
    }
}



#Preview {
    ContentView().frame(width: 1280,height: 800)
}
