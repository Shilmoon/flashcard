//
//  flashcardUITests.swift
//  flashcardUITests
//
//  Created by jhon on 15.04.2024.
//

import XCTest

class ContentViewUITests: XCTestCase {
    
    private var app: XCUIApplication!
    
    override func setUpWithError() throws {
        continueAfterFailure = false
        app = XCUIApplication()
        app.launch()
    }
    
    override func tearDownWithError() throws {
        app = nil
        
    }
    func testButtonTaps() throws {
        let learnButton = app.buttons["Learn new words"]
        let reviewButton = app.buttons["Review words"]
        XCTAssertTrue(learnButton.exists)
        XCTAssertTrue(reviewButton.exists)
        learnButton.tap()
        reviewButton.tap()
    }
    func testDailyGoalTextAppearance() {
        let dailyGoalText = app.staticTexts["Daily goal"]
        XCTAssertTrue(dailyGoalText.exists, "The text 'Daily goal' should exist.")
    }
    
    func testButtonShowsSliderOnTap() {
        let button = app.buttons["Edit Goal"]
        button.tap()
        let slider = app.sliders["goalSlider"]
        XCTAssertTrue(slider.exists, "The slider should be visible after tapping the button.")
        
    }
    func testSliderFunctionality() {
        let editGoalButton = app.buttons["Edit Goal"]
        if editGoalButton.exists {
            editGoalButton.tap()
            let slider = app.sliders["goalSlider"]
            if slider.exists {
                let initialValue = slider.value as! Double
                slider.adjust(toNormalizedSliderPosition: 0.5)
                let modifiedValue = slider.value as! Double
                XCTAssertNotEqual(initialValue, modifiedValue, "Slider value should have changed.")
            }
        }
    }
}
